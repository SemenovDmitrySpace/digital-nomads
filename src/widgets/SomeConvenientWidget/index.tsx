import React from 'react'

interface IProps {
  data: string | null
}

const SomeConvenientWidget: React.FC<IProps> = ({ data }) => {

  if (!data) {
    return null
  }

  return (
    <div
      style={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        height: 200,
        border: '1px solid gray',
        backgroundColor: 'lightgray',
      }}
    >
      { data }
    </div>
  )
}

export default SomeConvenientWidget
