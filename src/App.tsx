import React from 'react'

import ApiLoader from 'components/ApiLoader'
import Container from 'components/Container'
import SomeConvenientWidget from 'widgets/SomeConvenientWidget'
import TranslationsProvider from 'contexts/translations'
import useWidgetApi from 'hooks/useWidgetApi'

import './app.css'

const App: React.FC = () => {

  const { data, loaded } = useWidgetApi()

  return (
    <TranslationsProvider>
      <Container>
        <ApiLoader loaded={loaded} stepTimer={1000}>
          <SomeConvenientWidget data={data} />
        </ApiLoader>
      </Container>
    </TranslationsProvider>
  )
}

export default App
