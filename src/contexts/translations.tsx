import React, { createContext, useCallback, ReactNode } from 'react'

interface ITranslationsContext {
  getMessage: (key: string) => string
}

interface IProps {
  children: ReactNode
}

const messages: Record<string, string> = {
  'Loading.First': 'Виджет грузится',
  'Loading.Second': 'Виджет ещё грузится',
  'Loading.Third': 'Загрузка идёт дольше чем обычно. Пожалуйста, подождите',
  'Error.Timeout': 'Ошибка при загрузке. Пожалуйста -- обновите окно',
  'Success.LoadingFinished': 'Виджет загружен!',
}

export const TranslationsContext = createContext<ITranslationsContext>({} as ITranslationsContext)

const Provider: React.FC<IProps> = ({ children }) => {

  const getMessage = useCallback((key: string) => messages[key] ?? '', [])

  return (
    <TranslationsContext.Provider value={{ getMessage }}>
      { children }
    </TranslationsContext.Provider>
  )
}

export default Provider
