
const promiseSleep = <T>(resolveValue: T, delay: number = 500): Promise<T> => (
  new Promise((resolve) => {
    setTimeout(() => resolve(resolveValue), delay)
  })
)

export default promiseSleep
