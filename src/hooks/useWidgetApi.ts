import { useState, useEffect } from 'react'

import promiseSleep from 'utils/promiseSleep'

function getWidgetData(): Promise<string | null> {
  // Что бы протестировать поменяйте время
  return promiseSleep('My awesome widget data', 3000)
}

const useWidgetApi = () => {

  const [data, setData] = useState<string | null>(null)
  const [error, setError] = useState<string | null>(null)
  const [loading, setLoading] = useState<boolean>(false)
  const [loaded, setLoaded] = useState<boolean>(false)

  useEffect(() => {
    setLoading(true)
    getWidgetData()
      .then(setData)
      .catch(() => setError('Error'))
      .finally(() => {
        setLoading(false)
        setLoaded(true)
      })
  }, [])

  return { data, error, loading, loaded }
}

export default useWidgetApi
