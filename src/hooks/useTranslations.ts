import { useContext } from 'react'

import { TranslationsContext } from 'contexts/translations'

const useTranslations = () => useContext(TranslationsContext)

export default useTranslations
