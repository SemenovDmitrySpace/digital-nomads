import React, { ReactNode } from 'react';

interface IProps {
  children:  ReactNode
}

const Container: React.FC<IProps> = ({ children }) => {
  return (
    <div className='container'>
      { children }
    </div>
  )
}

export default Container
