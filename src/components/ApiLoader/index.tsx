import React, { ReactNode, useState, useEffect } from 'react'

import useTranslations from 'hooks/useTranslations'
import Text from './Text'

interface IProps {
  loaded: boolean
  stepTimer: number
  children: ReactNode
}

type TStep = 'first' | 'second' | 'third'

const apiStep: Record<TStep, string> = {
  first: 'Loading.First',
  second: 'Loading.Second',
  third: 'Loading.Third',
}

const ApiLoader: React.FC<IProps> = ({ loaded, stepTimer, children }) => {

  const { getMessage } = useTranslations()

  const [ time, setTime ] = useState<number>(0)
  const [ step, setStep ] = useState<TStep>('first')
  const [ hasError, setHasError ] = useState<boolean>(false)

  const totalTime = stepTimer * 3

  useEffect(() => {

    if (loaded) return
    if (time > totalTime) return

    const timerId = setInterval(() => {

      setTime((prevState) => prevState + stepTimer)

      if (step === 'first') {
        setStep('second')
      }
      if (step === 'second') {
        setStep('third')
      }
      if (step === 'third') {
        setHasError(true)
      }

    }, stepTimer);

    return () => { clearInterval(timerId) }
  }, [totalTime, stepTimer, step, time, loaded])

  if (hasError) {
    return <Text>{ getMessage('Error.Timeout') }</Text>
  }

  if (!loaded) {
    return (
      <>
        <div className='spinner'>
          <div className='double-bounce1'></div>
          <div className='double-bounce2'></div>
        </div>
        <Text>{ getMessage(apiStep[step]) }</Text>
      </>
    )
  }

  return (
    <>
      { children }
      <Text>{ getMessage('Success.LoadingFinished') }</Text>
    </>
  )
}

export default ApiLoader
