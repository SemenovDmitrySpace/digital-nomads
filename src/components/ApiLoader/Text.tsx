import React, { ReactNode } from 'react'

interface IProps { 
  children: ReactNode 
}

const Text: React.FC<IProps> = ({ children }) => (
  <p className='spinner__text'>
    { children }
  </p>
)

export default Text
